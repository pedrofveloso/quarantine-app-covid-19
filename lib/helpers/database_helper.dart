import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper._internal();

  factory DatabaseHelper() {
    return _instance;
  }

  DatabaseHelper._internal();

  Future<Database> _openDB() async {
    return openDatabase(
      join(await getDatabasesPath(), 'walk_around_database.db'),
      onCreate: (db, version) {
        return db.execute(
          "CREATE TABLE walk_around(id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, detail TEXT)",
        );
      },
      version: 1,
    );
  }

  Future<void> insert({String tableName, Map<String, dynamic> object}) async {
    final Database db = await _openDB();
    await db.insert(
      tableName,
      object,
      conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

   Future<List<Map<String, dynamic>>> getAll({String tableName, String column, String orderBy = 'DESC'}) async {
    final Database db = await _openDB();
    return await db.query(tableName, orderBy: '$column $orderBy');
  }

  Future<void> delete({int id, String tableName}) async {
    final db = await _openDB();
    await db.delete(
      tableName,
      where: "id = ?",
      whereArgs: [id],
    );
  }
}