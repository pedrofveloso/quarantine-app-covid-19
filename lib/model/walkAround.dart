class WalkAround {
  int id;
  DateTime date;
  String detail;

  WalkAround({this.id, this.date, this.detail});

  Map<String, dynamic> toMap() {
    return {
      'date': date.toString(),
      'detail': detail,
    };
  }
}
