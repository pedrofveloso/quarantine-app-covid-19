import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:quarantine_app/model/walkAround.dart';
import '../helpers/database_helper.dart';

class AddWalkAround extends StatefulWidget {
  AddWalkAround({Key key, this.model}) : super(key: key);
  final WalkAround model;
  @override
  _AddWalkAroundState createState() => _AddWalkAroundState(model: model);
}

class _AddWalkAroundState extends State<AddWalkAround> {
  final format = DateFormat('dd/MM/yyyy');
  final _formKey = GlobalKey<FormState>();
  WalkAround model;
  
  _AddWalkAroundState({WalkAround model}) {
      this.model = model ?? WalkAround(id: 0, date: DateTime.now(), detail: null);
  }

  void insertWalkAraound() async {
    await DatabaseHelper().insert(tableName: 'walk_around', object: this.model.toMap());
  }

  @override
  Widget build(BuildContext context) {
    var _dateTimeInputController = TextEditingController(text: format.format(this.model.date));
    return Scaffold(
      appBar: AppBar(title: Text('Adicionar saída')),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              DateTimeField(resetIcon: null,
                format: format,
                decoration: _inputDecorationStyle(text: 'Data de saída'),
                controller: _dateTimeInputController,
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(2020),
                      initialDate: DateTime.now(),
                      lastDate: DateTime.now());
                },
                onChanged: (value) {
                  this.model.date = value;
                },
              ),
              SizedBox(height: 16.0),
              TextFormField(
                decoration: _inputDecorationStyle(
                    text: 'Pra onde que tu foi, mizera?'),
                validator: (value) {
                  return value.isEmpty
                      ? 'Precisa dizer onde diabo tu foi, peste!'
                      : null;
                },
                onChanged: (value) {
                  this.model.detail = value;
                },
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                      height: 48,
                      width: double.infinity,
                      child: FlatButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            insertWalkAraound();
                            Navigator.pop(context);
                          }
                        },
                        color: Colors.indigo,
                        child: Text("Adicionar"),
                        textColor: Colors.white,
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration _inputDecorationStyle({String text}) {
    return InputDecoration(
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(4.0)),
      labelText: text,
      fillColor: Colors.white,
    );
  }
}
