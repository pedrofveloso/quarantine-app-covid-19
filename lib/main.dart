import 'package:flutter/material.dart';
import 'home/homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '40tena App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.indigo,
      ),
      home: MyHomePage(title: 'Sua Quarentena'),
    );
  }
}
