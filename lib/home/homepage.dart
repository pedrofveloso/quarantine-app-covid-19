import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:quarantine_app/helpers/database_helper.dart';
import 'package:quarantine_app/model/walkAround.dart';
import '../add-walk-around/add_walk_around.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void _addWalkAround(BuildContext context) async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddWalkAround()),
    );
    setState(() {});
  }

  Future<List<WalkAround>> fetchDB() async {
    final rawList =
        await DatabaseHelper().getAll(tableName: 'walk_around', column: 'date');
    return List.generate(rawList.length, (index) {
      DateFormat formatter = DateFormat('yyyy-MM-dd');
      return WalkAround(
        id: rawList[index]['id'],
        date: formatter.parse(rawList[index]['date']),
        detail: rawList[index]['detail'],
      );
    });
  }

  int numberOfDays(WalkAround model) {
    return DateTime.now().difference(model.date).inDays;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: FutureBuilder(
            future: fetchDB(),
            builder: (context, AsyncSnapshot<List<WalkAround>> snapshot) {
              List<Widget> children;
              if (snapshot.hasData && snapshot.data.length > 0) {
                children = <Widget>[
                  Text(
                    'Você está há',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  Text(
                    '${numberOfDays(snapshot.data.first)} dias',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
                  ),
                  Text(
                    'sem sair de casa',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  SizedBox(height: 8.0),
                  SymptomsDisclaimer(
                    numberOfDays: numberOfDays(snapshot.data.first),
                  ),
                  SizedBox(height: 8.0),
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, position) {
                        WalkAround model = snapshot.data[position];
                        final formatter = DateFormat('dd/MM/yyyy');
                        return WalkAroundCard(
                          model: model,
                          formatter: formatter,
                          state: this,
                        );
                      },
                    ),
                  )
                ];
              } else if (snapshot.hasError ||
                  (snapshot.hasData && snapshot.data.isEmpty)) {
                children = <Widget>[
                  Icon(
                    Icons.insert_emoticon,
                    color: Colors.indigo,
                    size: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('Você não tem saídas cadastradas!'),
                  )
                ];
              } else {
                children = <Widget>[
                  SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Carregando...'),
                  )
                ];
              }
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: children,
                ),
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _addWalkAround(context),
          tooltip: 'Adicione uma data de saída',
          child: Icon(Icons.add)),
      bottomNavigationBar: BottomAppBar(
        child: SizedBox(
          height: 48.0,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class SymptomsDisclaimer extends StatelessWidget {
  const SymptomsDisclaimer({Key key, this.numberOfDays}) : super(key: key);

  final int numberOfDays;

  @override
  Widget build(BuildContext context) {
    if (numberOfDays >= 2 && numberOfDays <= 14) {
      return Text(
        'Os sintomas aparecem entre 2 e 14 dias.\nSe sentir qualquer sintoma do COVID-19 procure fazer o teste.',
        style: TextStyle(color: Colors.amber),
        textAlign: TextAlign.center,
      );
    } else {
      return Text('');
    }
  }
}

class WalkAroundCard extends StatelessWidget {
  const WalkAroundCard(
      {Key key,
      @required this.model,
      @required this.formatter,
      @required this.state})
      : super(key: key);

  final WalkAround model;
  final DateFormat formatter;
  final _MyHomePageState state;

  void _deleteWalkAround(int id) async {
    await DatabaseHelper().delete(id: id, tableName: 'walk_around');
    this.state.setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text('${model.detail}'),
        subtitle: Text(formatter.format(model.date)),
        trailing: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            _deletionAlert(context, model);
          },
        ),
      ),
    );
  }

  Future<void> _deletionAlert(BuildContext context, WalkAround model) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Remover saída'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                    'Deseja deletar a saída para ${model.detail} no dia ${formatter.format(model.date)}'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Deletar'),
              onPressed: () {
                _deleteWalkAround(model.id);
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
